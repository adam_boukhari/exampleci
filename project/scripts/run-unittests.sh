echo "Running unit tests with coverage stats"
coverage run --omit */test_*,*__init__.py -m unittest discover -s project -v
if [ $? -ne 0 ]; then
	echo "Unit tests failed"
	exit 1
fi

